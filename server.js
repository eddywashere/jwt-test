var express = require('express');
var faker = require('faker');
var cors = require('cors');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var uuid = require('node-uuid');

var jwtSecret = 'fjkdlsajfoew239053/3uk';

var user = {
  username: 'u',
  password: 'p'
};

var app = express();

app.use(cors());
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(expressJwt({ secret: jwtSecret }).unless({ path: [ '/login' ]}));

app.get('/random-user', function (req, res) {
  console.log(req.user);
  var user = faker.helpers.userCard();
  user.avatar = faker.image.avatar();
  res.json(user);
});

// sign token
app.post('/login', authenticate, function (req, res) {
  var token = jwt.sign({
    iss: 'myApp',
    exp: Math.round(new Date() / 1000) + (3600), // 1 hr
    username: user.username,
    jti: uuid.v4()
  }, jwtSecret);
  res.send({
    token: token,
    user: user
  });
});

app.get('/me', jwtExtra, function (req, res) {
  console.log(req.user);
  res.send(req.user);
});

app.listen(3000, function () {
  console.log('App listening on localhost:3000');
});

// UTIL FUNCTIONS

// extra validation for jwt
function jwtExtra(req, res, next) {
  var err = null;

  // console.log(req.user.exp);
  console.log(new Date());
  console.log(new Date(req.user.exp * 1000));
  //

  // check if req.user.jti exists in blacklist
  if(req.user && req.user.jti){
    console.log('foooooo ' + req.user.jti);
    // do real stuff eventually
  } else {
    err = new Error('auth stuff failed');
  }
  next(err);
}

// validate username and pass
function authenticate(req, res, next) {
  var body = req.body;
  if (!body.username || !body.password) {
    res.status(400).end('Must provide username or password');
  } else if (body.username !== user.username || body.password !== user.password) {
    res.status(401).end('Username or password incorrect');
  } else {
    next();
  }
}
